﻿using Moq;
using NUnit.Framework;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Services;
using PerfectChannel.Checkout.Applicatoin.Models;
using PerfectChannel.Checkout.Applicatoin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;

namespace PerfectChannel.Checkout.Tests.Services
{
    [TestFixture]
    [Category("Services")]
    public class BasketServiceTests
    {
        private Mock<IBasketRepository> basketRepositoryMock;
        private Mock<ICatalogRepository> catalogRepositoryMock;
        private Mock<IInvoiceRepository> invoiceRepositoryMock;

        [SetUp]
        public void Setup()
        {
            basketRepositoryMock = new Mock<IBasketRepository>();
            catalogRepositoryMock = new Mock<ICatalogRepository>();
            invoiceRepositoryMock = new Mock<IInvoiceRepository>();
        }

        [Test]
        [TestCase("user", true, null)]
        [TestCase("user", false, "Basket not found for user")]
        public void BasketService_GetBasket_Tests(string userName, bool basketExists, string expectedMessage)
        {
            // Arrange
            if (basketExists)
            {
                basketRepositoryMock.Setup(x => x.GetUserBasket(userName)).Returns(new Basket() { User = userName });
            }
            else
            {
                basketRepositoryMock.Setup(x => x.GetUserBasket(userName)).Returns((Basket)null);
            }

            // Act
            var service = GetService();

            try
            {
                var result = service.GetBasketByUser(userName);
                // Assert
                Assert.That(result, Is.Not.Null);
                Assert.That(result.User, Is.EqualTo(userName));
            }
            catch (Exception ex)
            {
                // Assert
                Assert.That(ex, Is.TypeOf(typeof(InstanceNotFoundException)));
                Assert.That(ex.Message, Is.EqualTo(expectedMessage));
            }
        }



        [Test]
        [TestCase("user", false)]
        [TestCase("user", true)]
        public void BasketService_AddItemsToEmptyBasket_Tests(string userName, bool basketExists)
        {
            // Arrange
            var emptyBasket = new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName,
                Items = new List<BasketItem>()
            };

            if (basketExists)
            {
                basketRepositoryMock.Setup(x => x.GetUserBasket(userName)).Returns(emptyBasket);
            }
            else
            {
                basketRepositoryMock.Setup(x => x.GetUserBasket(userName)).Returns((Basket)null);
            }
            basketRepositoryMock.Setup(x => x.CreateUserBasket(userName)).Returns(emptyBasket);

            var apples = "Apples";
            var milk = "Milk";
            catalogRepositoryMock.Setup(x => x.GetProductByName(apples)).Returns(new Product() { Id = 1, Name = apples, Stock = 2 });
            catalogRepositoryMock.Setup(x => x.GetProductByName(milk)).Returns(new Product() { Id = 2, Name = milk, Stock = 10 });
            catalogRepositoryMock.Setup(x => x.IsInStock(It.IsAny<Product>(), It.IsAny<int>())).Returns(true);

            // Act
            var service = GetService();
            var itemsToAdd = new List<AddItemRequest>()
            {
                new AddItemRequest() {ItemName = apples, Quantity = 2 },
                new AddItemRequest() {ItemName = milk, Quantity = 5 }

            };
            var result = service.AddItemsToUserBasket(userName, itemsToAdd);

            // Assert
            catalogRepositoryMock.Verify(x => x.GetProductByName(It.IsAny<string>()), Times.Exactly(2));
            catalogRepositoryMock.Verify(x => x.IsInStock(It.IsAny<Product>(), It.IsAny<int>()), Times.Exactly(2));
            Assert.That(result, Is.Not.Null);
            Assert.That(result.User, Is.EqualTo(userName));
            Assert.That(result.Items, Is.Not.Null);
            Assert.That(result.Items.Count, Is.EqualTo(2));

        }

        [TestCase("user")]
        public void BasketService_AddItemsToExistingBasket_Tests(string userName)
        {
            // Arrange
            var productApple = new Product() { Id = 1, Name = "Apples", Description = "Fruit", Price = 1.00m, Stock = 2 };
            var productMilk = new Product() { Id = 2, Name = "Milk", Description = "Milk", Price = 1.05m, Stock = 20 };

            var filledBasket = new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName,
                Items = new List<BasketItem>()
                {
                    new BasketItem() {Id = Guid.NewGuid(), ProductId = productApple.Id, Description = productApple.Name, Quantity = 2 },
                    new BasketItem() {Id = Guid.NewGuid(), ProductId = productMilk.Id, Description = productMilk.Name, Quantity = 2 }
                }
            };
            basketRepositoryMock.Setup(x => x.GetUserBasket(userName)).Returns(filledBasket);

            catalogRepositoryMock.Setup(x => x.GetProductByName(productApple.Name)).Returns(new Product() { Id = 1, Name = productApple.Name, Stock = 2 });
            catalogRepositoryMock.Setup(x => x.GetProductByName(productMilk.Name)).Returns(new Product() { Id = 2, Name = productMilk.Name, Stock = 10 });
            catalogRepositoryMock.Setup(x => x.IsInStock(It.IsAny<Product>(), It.IsAny<int>()))
                .Returns((Product p, int q) => p.Id == productApple.Id ? true : false);

            // Act
            var service = GetService();
            var itemsToAdd = new List<AddItemRequest>()
            {
                new AddItemRequest() {ItemName = productApple.Name, Quantity = 2 },
                new AddItemRequest() {ItemName = productMilk.Name, Quantity = 5 }

            };
            var result = service.AddItemsToUserBasket(userName, itemsToAdd);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.User, Is.EqualTo(userName));
            Assert.That(result.Items, Is.Not.Null);
            Assert.That(result.Items.Count, Is.EqualTo(2));
            var itemsWithError = result.Items.Where(x => x.HasError);
            Assert.That(itemsWithError, Is.Not.Null);
            Assert.That(itemsWithError.Count(), Is.EqualTo(1));
            var itemWithError = itemsWithError.FirstOrDefault();
            Assert.That(itemWithError.ProductId, Is.EqualTo(productMilk.Id));
            Assert.That(itemWithError.Quantity, Is.EqualTo(2));
            Assert.That(itemWithError.ErrorMessage, Is.EqualTo("Out of stock"));
        }

        [Test]
        [TestCase("user", true)]
        [TestCase("user", false)]
        public void BasketService_Checkout_Tests(string userName, bool basketExists)
        {
            // Arrange
            var productApple = new Product() { Id = 1, Name = "Apples", Price = 1.05m, Stock = 2 };
            var productMilk = new Product() { Id = 2, Name = "Milk", Price = 1.00m, Stock = 2 };

            var filledBasketAvailableInventory = new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName,
                Items = new List<BasketItem>()
                {
                    new BasketItem() { Id = Guid.NewGuid(), ProductId = productApple.Id, Price = productApple.Price, Quantity = 2 },
                    new BasketItem() { Id = Guid.NewGuid(), ProductId = productMilk.Id, Price = productMilk.Price, Quantity = 2 },
                }
            };
            if (basketExists)
            {
                basketRepositoryMock.Setup(x => x.GetUserBasket(userName)).Returns(filledBasketAvailableInventory);
            }
            else
            {
                basketRepositoryMock.Setup(x => x.GetUserBasket(userName)).Returns((Basket)null);
            }
            catalogRepositoryMock.Setup(x => x.GetProductById(productApple.Id)).Returns(productApple);
            catalogRepositoryMock.Setup(x => x.GetProductById(productMilk.Id)).Returns(productMilk);
            catalogRepositoryMock.Setup(x => x.IsInStock(productApple, 2)).Returns(true);
            catalogRepositoryMock.Setup(x => x.IsInStock(productMilk, 2)).Returns(true);

            invoiceRepositoryMock.Setup(x => x.AddInvoice(It.IsAny<Invoice>())).Returns((Invoice i) => i);

            // Act
            var service = GetService();

            try
            {
                var result = service.Checkout(userName);
                catalogRepositoryMock.VerifyAll();
                basketRepositoryMock.VerifyAll();
                invoiceRepositoryMock.VerifyAll();
                Assert.That(result, Is.Not.Null);
                Assert.That(result.Id, Is.Not.Null);
                Assert.That(result.Items, Is.Not.Null);
                Assert.That(result.Items.Count, Is.EqualTo(2));
            }
            catch (Exception ex)
            {
                if (!basketExists)
                {
                    Assert.That(ex, Is.TypeOf(typeof(InstanceNotFoundException)));
                }
                else
                {
                    Assert.IsFalse(true);
                }
            }
        }


        private IBasketService GetService()
        {
            return new BasketService(basketRepositoryMock.Object, catalogRepositoryMock.Object, invoiceRepositoryMock.Object);
        }
    }
}