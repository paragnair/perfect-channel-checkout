﻿using Moq;
using NUnit.Framework;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories;
using PerfectChannel.Checkout.Applicatoin.Models;
using PerfectChannel.Checkout.Applicatoin.Services;
using System.Collections.Generic;

namespace PerfectChannel.Checkout.Tests.Services
{
    [TestFixture]
    [Category("Services")]
    public class CatalogServiceTests
    {
        [Test]
        public void CatalogService_ListProducts_ListsProducts()
        {
            // Arrange
            Mock<ICatalogRepository> catalogRepositoryMock = new Mock<ICatalogRepository>();
            var products = new List<Product>()
            {
                new Product() { Id = 1, Name = "A", Description = "AA", Price = 1.00m, Stock = 10 },
                new Product() { Id = 2, Name = "B", Description = "BB", Price = 2.00m, Stock = 5 }
            };
            catalogRepositoryMock.Setup(x => x.GetProducts()).Returns(products);

            // Act
            var catalogService = new CatalogService(catalogRepositoryMock.Object);
            var results = catalogService.ListProducts();

            // Assert
            catalogRepositoryMock.VerifyAll();
            Assert.That(results, Is.Not.Null);
            Assert.That(results.Count, Is.EqualTo(products.Count));
            Assert.That(results, Is.EqualTo(products));
        }
    }
}
