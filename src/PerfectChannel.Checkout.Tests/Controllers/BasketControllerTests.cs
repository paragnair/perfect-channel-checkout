﻿using Moq;
using NUnit.Framework;
using PerfectChannel.Checkout.API.Controllers;
using PerfectChannel.Checkout.API.Models.Requests;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Services;
using PerfectChannel.Checkout.Applicatoin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;

namespace PerfectChannel.Checkout.Tests.Controllers
{
    [TestFixture]
    [Category("Controllers")]
    public class BasketControllerTests
    {
        private Mock<IBasketService> basketServiceMock;

        [SetUp]
        public void Setup()
        {
            basketServiceMock = new Mock<IBasketService>();
        }

        [Test]
        [TestCase("user", "Apples", true, null)]
        public void BasketController_AddSingleItem_Tests(string userName, string itemName, bool success, string message)
        {
            // Arrange
            var request = new AddToBasketRequest() { User = userName, Item = itemName };
            var basket = new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName,
                Items = new List<BasketItem>()
                {
                    new BasketItem() { Description = itemName, Id = Guid.NewGuid(), Price = 1.03m, ProductId = 1, Quantity = 1 }
                }
            };
            basketServiceMock.Setup(x => x.AddItemsToUserBasket(userName, It.IsAny<List<AddItemRequest>>())).Returns(basket);

            // Act
            var basketController = GetBasketController();
            var result = basketController.AddItemsToUserCart(request);

            // Assert
            basketServiceMock.VerifyAll();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.EqualTo(success));
            Assert.That(result.Message, Is.EqualTo(message));
            if (success)
            {
                Assert.That(result.Basket, Is.EqualTo(basket));
            }
        }

        [Test]
        [TestCase("user", true, true, null)]
        [TestCase("user", false, false, "Basket not found.")]
        public void BasketController_GetBasket_Tests(string userName, bool basketExists, bool expectedSuccess,
            string expectedMessage)
        {
            // Arrange
            var existingBasket = new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName,
                Items = new List<BasketItem>()
                {
                    BasketItem.Create(new Product() { Id = 1, Name = "Apples", Description = "Fruits", Price = 1.00m, Stock = 2 }, 2)
                }
            };
            if (basketExists)
            {
                basketServiceMock.Setup(x => x.GetBasketByUser(userName)).Returns(existingBasket);
            }
            else
            {
                basketServiceMock.Setup(x => x.GetBasketByUser(userName)).Returns((Basket)null);
            }

            // Act
            var basketController = GetBasketController();
            var result = basketController.GetBasketByUser(userName);

            // Assert
            basketServiceMock.VerifyAll();
            Assert.That(result, Is.Not.Null);
            if (basketExists)
            {
                Assert.That(result.Success, Is.EqualTo(expectedSuccess));
                Assert.That(result.Message, Is.EqualTo(expectedMessage));
            }
        }

        [Test]
        [TestCase("user")]
        public void BasketController_AddMultipleItems_AllOk(string userName)
        {
            // Arrange
            var basket = new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName,
                Items = new List<BasketItem>()
                {
                    new BasketItem() { Id = Guid.NewGuid(), Description = "Apples", Price = 1.50m, Quantity = 2 },
                    new BasketItem() { Id = Guid.NewGuid(), Description = "Milk", Price = 1.00m, Quantity = 1 }
                }
            };
            basketServiceMock.Setup(x => x.AddItemsToUserBasket(userName, It.IsAny<List<AddItemRequest>>())).Returns(basket);

            // Act
            var basketController = GetBasketController();
            var request = new AddToBasketRequest()
            {
                User = userName,
                MultipleItems = true,
                Items = new List<AddToBasketRequestItem>()
                {
                    new AddToBasketRequestItem() { ItemName = "Apples", Quantity = 2 },
                    new AddToBasketRequestItem() { ItemName = "Milk", Quantity = 1 }
                }
            };
            var result = basketController.AddItemsToUserCart(request);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Null);
            Assert.That(result.Basket, Is.Not.Null);
            Assert.That(result.Basket.User, Is.EqualTo(userName));
            Assert.That(result.Basket.Items, Is.Not.Null);
            Assert.That(result.Basket.Items.Count, Is.EqualTo(request.Items.Count));
        }

        [Test]
        [TestCase("user")]
        public void BasketController_AddMultipleItems_SomeErrors(string userName)
        {
            // Arrange
            var productApple = new Product() { Id = 1, Name = "Apples", Price = 1.50m };
            var productMilk = new Product() { Id = 2, Name = "Milk", Price = 1.00m };

            var basket = new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName,
                Items = new List<BasketItem>()
                {
                    new BasketItem() { Id = Guid.NewGuid(), ProductId = productApple.Id, Description = productApple.Name, Price = productApple.Price, Quantity = 2 },
                    new BasketItem() { Id = Guid.NewGuid(), ProductId = productMilk.Id, Description = productMilk.Name, Price = productMilk.Price, Quantity = 1, HasError = true, ErrorMessage = "Not in stock" }
                }
            };
            basketServiceMock.Setup(x => x.AddItemsToUserBasket(userName, It.IsAny<List<AddItemRequest>>())).Returns(basket);

            // Act
            var basketController = GetBasketController();
            var request = new AddToBasketRequest()
            {
                User = userName,
                MultipleItems = true,
                Items = new List<AddToBasketRequestItem>()
                {
                    new AddToBasketRequestItem() { ItemName = productApple.Name, Quantity = 2 },
                    new AddToBasketRequestItem() { ItemName = productMilk.Name, Quantity = 1 }
                }
            };
            var result = basketController.AddItemsToUserCart(request);

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Null);
            Assert.That(result.Basket, Is.Not.Null);
            Assert.That(result.Basket.User, Is.EqualTo(userName));
            Assert.That(result.Basket.Items, Is.Not.Null);
            Assert.That(result.Basket.Items.Count, Is.EqualTo(request.Items.Count));
            var erroredItems = result.Basket.Items.Where(x => x.HasError);
            Assert.That(erroredItems, Is.Not.Null);
            Assert.That(erroredItems.Count(), Is.EqualTo(1));
            var erroredItem = erroredItems.FirstOrDefault();
            Assert.That(erroredItem.ProductId, Is.EqualTo(productMilk.Id));
            Assert.That(erroredItem.ErrorMessage, Is.EqualTo("Not in stock"));
        }

        [Test]
        [TestCase("user", true)]
        [TestCase("user", false)]
        public void BasketController_Checkout_Tests(string user, bool basketExists)
        {
            // Arrange
            var productApples = Product.CreateNew(1, "Apples", "Fruits", 1.05m, 100);
            var productMilk = Product.CreateNew(2, "Milk", "Milk", 1.00m, 100);
            var productPhone = Product.CreateNew(3, "Phone", "Telecom", 150.00m, 100);


            var filledBasket = new Basket()
            {
                Id = Guid.NewGuid(),
                User = user,
                Items = new List<BasketItem>()
                {
                    new BasketItem() { ProductId = productApples.Id, Description = productApples.Name, Price = productApples.Price, Quantity = 2 },
                    new BasketItem() { ProductId = productMilk.Id, Description = productMilk.Name, Price = productMilk.Price, Quantity = 2 },
                    new BasketItem() { ProductId = productPhone.Id, Description = productPhone.Name, Price = productPhone.Price, Quantity = 0, HasError = true, ErrorMessage = "Out of Stock" }
                }
            };

            if (basketExists)
            {
                basketServiceMock.Setup(x => x.Checkout(user)).Returns(new Invoice()
                {
                    User = user,
                    Items = new List<InvoiceItem>()
                    {
                        new InvoiceItem() { Description = productApples.Name, Price = productApples.Price, Quantity = 2 },
                        new InvoiceItem() { Description = productMilk.Name, Price = productMilk.Price, Quantity = 2 }
                    }
                });
            }
            else
            {
                basketServiceMock.Setup(x => x.Checkout(user)).Throws(new InstanceNotFoundException());
            }

            // Act
            var basketController = GetBasketController();
            var result = basketController.Checkout(user);

            // Assert
            basketServiceMock.VerifyAll();
            Assert.That(result, Is.Not.Null);
            if (basketExists)
            {
                Assert.That(result.Success, Is.True);
                Assert.That(result.Message, Is.Null);
                Assert.That(result.Invoice, Is.Not.Null);
            }
            else
            {
                Assert.That(result.Success, Is.False);
                Assert.That(result.Message, Is.EqualTo("Basket not found"));
                Assert.That(result.Invoice, Is.Null);
            }

        }

        private BasketController GetBasketController()
        {
            return new BasketController(basketServiceMock.Object);
        }
    }
}
