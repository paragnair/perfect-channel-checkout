﻿using Moq;
using NUnit.Framework;
using PerfectChannel.Checkout.API.Controllers;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Services;
using PerfectChannel.Checkout.Applicatoin.Models;
using System.Collections.Generic;

namespace PerfectChannel.Checkout.Tests.Controllers
{
    [TestFixture]
    [Category("Controllers")]
    public class ProductsControllerTests
    {
        [Test]
        public void ProductsController_List_RetursList()
        {
            // Arrange
            Mock<ICatalogService> catalogService = new Mock<ICatalogService>();
            var products = new List<Product>()
            {
                new Product() { Id = 1, Name = "A", Description = "AA", Price = 1.00m, Stock = 10 },
                new Product() { Id = 2, Name = "B", Description = "BB", Price = 2.00m, Stock = 5 }
            };
            catalogService.Setup(x => x.ListProducts()).Returns(products);

            // Act
            var productsController = new ProductsController(catalogService.Object);
            var results = productsController.GetProducts();

            // Assert
            catalogService.VerifyAll();
            Assert.That(results, Is.Not.Null);
            Assert.That(results.Count, Is.EqualTo(products.Count));
            Assert.That(results, Is.EqualTo(products));
        }
    }
}