﻿namespace PerfectChannel.Checkout.Applicatoin.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int Stock { get; set; }

        public static Product CreateNew(int id, string name, string description, decimal price, int inventory)
        {
            return new Product()
            {
                Id = id,
                Name = name,
                Description = description,
                Price = price,
                Stock = inventory
            };
        }
    }
}