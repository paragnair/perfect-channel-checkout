﻿namespace PerfectChannel.Checkout.Applicatoin.Models
{
    public class AddItemRequest
    {
        public string ItemName { get; set; }

        public int Quantity { get; set; }
    }
}
