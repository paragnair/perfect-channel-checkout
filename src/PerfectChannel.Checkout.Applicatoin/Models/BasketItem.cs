﻿using System;

namespace PerfectChannel.Checkout.Applicatoin.Models
{
    public class BasketItem
    {
        public Guid Id { get; set; }

        public string Description { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal TotalPrice
        {
            get { return Price * Quantity; }

        }

        public bool HasError { get; set; }

        public string ErrorMessage { get; set; }

        public static BasketItem Create(Product product, int quantity, bool hasErrors = false, string errorMessage = null)
        {
            return new BasketItem()
            {
                Id = Guid.NewGuid(),
                Description = product.Name,
                Price = product.Price,
                ProductId = product.Id,
                Quantity = quantity,
                HasError = hasErrors,
                ErrorMessage = errorMessage
            };
        }
    }
}