﻿using System;
using System.Collections.Generic;

namespace PerfectChannel.Checkout.Applicatoin.Models
{
    public class Basket
    {
        public Guid Id { get; set; }

        public List<BasketItem> Items { get; set; }

        public string User { get; set; }

        public Basket()
        {
            this.Items = new List<BasketItem>();
        }

        public static Basket Create(string userName)
        {
            return new Basket()
            {
                Id = Guid.NewGuid(),
                User = userName
            };
        }
    }
}
