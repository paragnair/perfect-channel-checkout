﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PerfectChannel.Checkout.Applicatoin.Models
{
    public class Invoice
    {
        public string User { get; set; }

        public List<InvoiceItem> Items { get; set; }

        public decimal Total
        {
            get { return this.Items?.Sum(x => x.Total) ?? 0.00m; }

        }

        public Guid Id { get; set; }
    }
}
