﻿using System;

namespace PerfectChannel.Checkout.Applicatoin.Models
{
    public class BasketException : Exception
    {
        public BasketException(string message) : base(message)
        {

        }
    }
}
