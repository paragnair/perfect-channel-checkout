﻿using PerfectChannel.Checkout.Applicatoin.Models;
using System.Collections.Generic;

namespace PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories
{
    public interface ICatalogRepository
    {
        List<Product> GetProducts();

        Product GetProductByName(string itemName);

        bool IsInStock(Product product, int quantity);

        Product GetProductById(int productId);

        void RemoveInventory(int productId, int quantity);
    }
}