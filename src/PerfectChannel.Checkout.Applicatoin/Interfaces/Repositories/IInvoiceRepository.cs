﻿using PerfectChannel.Checkout.Applicatoin.Models;

namespace PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories
{
    public interface IInvoiceRepository
    {
        Invoice AddInvoice(Invoice invoice);
    }
}