﻿using PerfectChannel.Checkout.Applicatoin.Models;
using System;

namespace PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories
{
    public interface IBasketRepository
    {
        Basket GetUserBasket(string userName);

        Basket CreateUserBasket(string userName);

        Basket AddToBasket(Basket basket, Product product, int quantity);

        Basket Save(Basket basket);

        void Delete(Guid basketId);
    }
}