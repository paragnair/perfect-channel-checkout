﻿using PerfectChannel.Checkout.Applicatoin.Models;
using System.Collections.Generic;

namespace PerfectChannel.Checkout.Applicatoin.Interfaces.Services
{
    public interface ICatalogService
    {
        List<Product> ListProducts();
    }
}