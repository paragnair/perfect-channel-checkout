﻿using PerfectChannel.Checkout.Applicatoin.Models;
using System.Collections.Generic;

namespace PerfectChannel.Checkout.Applicatoin.Interfaces.Services
{
    public interface IBasketService
    {
        Basket GetBasketByUser(string userName);

        Basket AddItemsToUserBasket(string userName, List<AddItemRequest> itemsToAdd);

        Invoice Checkout(string user);
    }
}