﻿using PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Services;
using PerfectChannel.Checkout.Applicatoin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;

namespace PerfectChannel.Checkout.Applicatoin.Services
{
    public class BasketService : IBasketService
    {
        private readonly IBasketRepository _basketRepository;
        private readonly ICatalogRepository _catalogRepository;
        private readonly IInvoiceRepository _invoiceRepository;

        public BasketService(IBasketRepository basketRepository, ICatalogRepository catalogRepository, IInvoiceRepository invoiceRepository)
        {
            _basketRepository = basketRepository;
            _catalogRepository = catalogRepository;
            _invoiceRepository = invoiceRepository;
        }

        public Basket GetBasketByUser(string userName)
        {
            var basket = _basketRepository.GetUserBasket(userName);
            if (basket == null)
            {
                throw new InstanceNotFoundException($"Basket not found for {userName}");
            }
            return basket;
        }

        public Basket AddItemsToUserBasket(string userName, List<AddItemRequest> itemsToAdd)
        {
            // get the basket or create one if it does not exist
            var basket = GetOrCreateBasketByUser(userName);

            foreach (var item in itemsToAdd)
            {
                basket = AddItemToUserBasket(basket, item.ItemName, item.Quantity);
            }

            return basket;
        }

        public Invoice Checkout(string user)
        {
            var basket = _basketRepository.GetUserBasket(user);

            if (basket == null)
            {
                throw new InstanceNotFoundException("Basket not found");
            }

            // remove zero quantity items
            basket.Items = basket.Items.Where(x => x.Quantity > 0)
                                .ToList();

            if (CheckAllItemsInStock(basket.Items))
            {
                return CreateInvoice(basket);
            }

            throw new BasketException("None of the items in basket are in stock");
        }

        private Invoice CreateInvoice(Basket basket)
        {
            // reduce inventory
            basket.Items.All(x =>
            {
                _catalogRepository.RemoveInventory(x.ProductId, x.Quantity);
                return true;
            });

            // build invoice
            var invoice = new Invoice()
            {
                Id = Guid.NewGuid(),
                User = basket.User,
                Items = CreateInvoiceItems(basket.Items)
            };

            // delete basket
            _basketRepository.Delete(basket.Id);

            // add the invoice
            invoice = _invoiceRepository.AddInvoice(invoice);

            return invoice;
        }

        private List<InvoiceItem> CreateInvoiceItems(List<BasketItem> items)
        {
            return items.Select(x => new InvoiceItem()
            {
                Description = x.Description,
                Quantity = x.Quantity,
                Price = x.Price
            })
                .ToList();
        }

        private bool CheckAllItemsInStock(List<BasketItem> items)
        {
            return items.All(x =>
            {
                var product = _catalogRepository.GetProductById(x.ProductId);

                return _catalogRepository.IsInStock(product, x.Quantity);
            });
        }

        private Basket AddItemToUserBasket(Basket basket, string itemName, int quantity)
        {
            // get product by name
            var product = _catalogRepository.GetProductByName(itemName);

            // check if the item already exists in basket
            var itemInBasket = GetItemFromBasket(basket, product);

            if (itemInBasket != null)
            {
                // update item in basket
                // get the already added quantity
                var existingQuantity = itemInBasket.Quantity;

                // check inventory
                if (_catalogRepository.IsInStock(product, existingQuantity + quantity))
                {
                    itemInBasket.Quantity += quantity;
                }
                else
                {
                    itemInBasket.HasError = true;
                    itemInBasket.ErrorMessage = "Out of stock";
                }
            }
            else
            {
                // add new item to basket
                basket = _catalogRepository.IsInStock(product, quantity)
                    ? AddToBasketWithoutCheck(basket, product, quantity)
                    : AddToBasketWithoutCheck(basket, product, 0, true, "Out of stock");
            }

            return basket;
        }

        private Basket AddToBasketWithoutCheck(Basket basket, Product product, int quantity, bool hasErrors = false, string errorMessage = null)
        {
            basket.Items.Add(BasketItem.Create(product, quantity, hasErrors, errorMessage));
            return basket;
        }

        private BasketItem GetItemFromBasket(Basket basket, Product product)
        {
            return basket.Items?.FirstOrDefault(x => x.ProductId == product.Id);
        }

        private Basket GetOrCreateBasketByUser(string userName)
        {
            // get user basket
            var basket = _basketRepository.GetUserBasket(userName) ?? _basketRepository.CreateUserBasket(userName);

            return basket;
        }

    }
}
