﻿using PerfectChannel.Checkout.Applicatoin.Interfaces.Services;
using PerfectChannel.Checkout.Applicatoin.Models;
using System.Collections.Generic;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories;

namespace PerfectChannel.Checkout.Applicatoin.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly ICatalogRepository _catalogRepository;


        public CatalogService(ICatalogRepository catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }

        public List<Product> ListProducts()
        {
            return _catalogRepository.GetProducts();
        }
    }
}