﻿using PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories;
using PerfectChannel.Checkout.Applicatoin.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PerfectChannel.Checkout.Applicatoin.Repositories
{
    public class InMemoryBasketRepository : IBasketRepository
    {
        private static List<Basket> _baskets;

        static InMemoryBasketRepository()
        {
            _baskets = new List<Basket>();
        }

        public Basket GetUserBasket(string userName)
        {
            return _baskets.FirstOrDefault(x => x.User.ToLower() == userName.ToLower());
        }

        public Basket CreateUserBasket(string userName)
        {
            var basket = Basket.Create(userName);
            _baskets.Add(basket);
            return basket;
        }

        public Basket AddToBasket(Basket basket, Product product, int quantity)
        {
            bool itemFound = false;
            if (basket.Items != null && basket.Items.Any())
            {
                // find the product in basket
                var basketItem = basket.Items.FirstOrDefault(x => x.ProductId == product.Id);
                if (basketItem != null)
                {
                    itemFound = true;
                    // incremente the quantity
                    basketItem.Quantity += quantity;
                }
            }

            if (!itemFound)
            {
                // add item
                basket.Items.Add(BasketItem.Create(product, quantity));
            }

            // save the basket
            basket = Save(basket);

            return basket;
        }

        public Basket Save(Basket basket)
        {
            var basketFromCollection = _baskets.FirstOrDefault(x => x.Id == basket.Id);
            basketFromCollection = basket;
            return basketFromCollection;
        }

        public void Delete(Guid basketId)
        {
            var basket = _baskets.FirstOrDefault(x => x.Id == basketId);
            _baskets.Remove(basket);
        }
    }
}