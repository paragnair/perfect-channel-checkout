﻿using PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories;
using PerfectChannel.Checkout.Applicatoin.Models;
using System.Collections.Generic;

namespace PerfectChannel.Checkout.Applicatoin.Repositories
{
    public class InMemoryInvoiceRepository : IInvoiceRepository
    {
        private static List<Invoice> _invoices;

        static InMemoryInvoiceRepository()
        {
            _invoices = new List<Invoice>();
        }

        public Invoice AddInvoice(Invoice invoice)
        {
            _invoices.Add(invoice);
            return invoice;
        }
    }
}
