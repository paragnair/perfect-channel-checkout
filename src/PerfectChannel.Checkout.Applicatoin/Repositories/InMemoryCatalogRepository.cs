﻿using PerfectChannel.Checkout.Applicatoin.Interfaces.Repositories;
using PerfectChannel.Checkout.Applicatoin.Models;
using System.Collections.Generic;
using System.Linq;

namespace PerfectChannel.Checkout.Applicatoin.Repositories
{
    public class InMemoryCatalogRepository : ICatalogRepository
    {
        private static List<Product> _products;

        static InMemoryCatalogRepository()
        {
            // initialize the intial list of products
            _products = new List<Product>()
            {
                Product.CreateNew(1, "Apples", "Fruit", 2.5m, 5),
                Product.CreateNew(2, "Bread", "Loaf", 1.35m, 10),
                Product.CreateNew(3, "Oranges", "Fruit", 2.99m, 10),
                Product.CreateNew(4, "Milk", "Milk", 2.07m, 3),
                Product.CreateNew(5, "Chocolate", "Bars", 1.79m, 20)
            };
        }

        public List<Product> GetProducts()
        {
            return _products;
        }

        public Product GetProductByName(string itemName)
        {
            return _products.FirstOrDefault(x => x.Name.ToLower() == itemName.ToLower());
        }

        public bool IsInStock(Product product, int quantity)
        {
            var productInDb = _products.FirstOrDefault(x => x.Id == product.Id);
            return productInDb == null || productInDb.Stock >= quantity;
        }

        public Product GetProductById(int productId)
        {
            return _products.FirstOrDefault(x => x.Id == productId);
        }

        public void RemoveInventory(int productId, int quantity)
        {
            var product = _products.FirstOrDefault(x => x.Id == productId);
            product.Stock -= quantity;
        }
    }
}
