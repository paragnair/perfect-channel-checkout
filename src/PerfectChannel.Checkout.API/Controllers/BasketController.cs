﻿using PerfectChannel.Checkout.API.Models.Requests;
using PerfectChannel.Checkout.API.Models.Responses;
using PerfectChannel.Checkout.Applicatoin.Interfaces.Services;
using PerfectChannel.Checkout.Applicatoin.Models;
using PerfectChannel.Checkout.Applicatoin.Repositories;
using PerfectChannel.Checkout.Applicatoin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Web.Http;

namespace PerfectChannel.Checkout.API.Controllers
{
    [RoutePrefix("api/basket")]
    public class BasketController : ApiController
    {
        private readonly IBasketService _basketService;

        public BasketController(IBasketService basketService)
        {
            _basketService = basketService;
        }

        // TODO: Inject with IOC
        public BasketController() : this(new BasketService(new InMemoryBasketRepository(), new InMemoryCatalogRepository(), new InMemoryInvoiceRepository()))
        {

        }

        // POST api/basket
        [HttpPost]
        [Route("")]
        public AddToBasketResponse AddItemsToUserCart(AddToBasketRequest request)
        {
            var basket = _basketService.AddItemsToUserBasket(request.User, CreateItemsRequest(request));

            return AddToBasketResponse.CreateSuccess(basket);
        }

        // GET api/basket/bob
        [Route("{userName}")]
        public GetBasketResponse GetBasketByUser(string userName)
        {
            try
            {
                var basket = _basketService.GetBasketByUser(userName);
                return GetBasketResponse.CreateSuccess(basket);
            }
            catch (InstanceNotFoundException)
            {
                return GetBasketResponse.CreateFailure("Basket not found.");
            }
            catch (Exception ex)
            {
                return GetBasketResponse.CreateFailure($"Basket not found. Error: {ex.Message}");
            }
        }

        // GET api/basket/bob/checkout
        [Route("{userName}/checkout")]
        [HttpGet]
        public BasketCheckoutResponse Checkout(string username)
        {
            try
            {
                var invoice = _basketService.Checkout(username);
                return BasketCheckoutResponse.CreateSuccess(invoice);
            }
            catch (InstanceNotFoundException)
            {
                return BasketCheckoutResponse.CreateFailure("Basket not found");
            }
        }

        private List<AddItemRequest> CreateItemsRequest(AddToBasketRequest request)
        {
            if (request.MultipleItems)
            {
                return CreateItemsRequest(request.Items);
            }
            else
            {
                return CreateItemsRequest(request.Item, 1);
            }
        }

        private List<AddItemRequest> CreateItemsRequest(string item, int quantity)
        {
            return new List<AddItemRequest>()
            {
                new AddItemRequest() { ItemName = item, Quantity = quantity }
            };
        }

        private List<AddItemRequest> CreateItemsRequest(List<AddToBasketRequestItem> items)
        {
            return items.Select(x => new AddItemRequest() { ItemName = x.ItemName, Quantity = x.Quantity }).ToList();
        }
    }
}
