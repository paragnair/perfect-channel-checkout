﻿using PerfectChannel.Checkout.Applicatoin.Interfaces.Services;
using PerfectChannel.Checkout.Applicatoin.Models;
using PerfectChannel.Checkout.Applicatoin.Repositories;
using PerfectChannel.Checkout.Applicatoin.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace PerfectChannel.Checkout.API.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductsController : ApiController
    {
        private readonly ICatalogService _catalogService;


        public ProductsController(ICatalogService catalogService)
        {
            _catalogService = catalogService;
        }

        // TODO: Remove this and have an IOC inject it
        public ProductsController() : this(new CatalogService(new InMemoryCatalogRepository()))
        {

        }

        // GET api/products
        [Route("")]
        public List<Product> GetProducts()
        {
            return _catalogService.ListProducts();
        }
    }
}
