﻿namespace PerfectChannel.Checkout.API.Models
{
    public abstract class StatusResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        protected virtual void MarkSuccess()
        {
            this.Success = true;
        }

        protected virtual void MarkFailure(string message)
        {
            this.Success = false;
            this.Message = message;
        }
    }
}