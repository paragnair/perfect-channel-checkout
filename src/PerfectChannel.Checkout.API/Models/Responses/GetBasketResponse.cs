﻿using PerfectChannel.Checkout.Applicatoin.Models;

namespace PerfectChannel.Checkout.API.Models.Responses
{
    public class GetBasketResponse : StatusResponse
    {
        public static GetBasketResponse CreateSuccess(Basket basket)
        {
            return new GetBasketResponse()
            {
                Success = true,
                Basket = basket
            };
        }

        public Basket Basket { get; set; }

        public static GetBasketResponse CreateFailure(string message)
        {
            return new GetBasketResponse()
            {
                Success = false,
                Message = message
            };
        }
    }
}