﻿using PerfectChannel.Checkout.Applicatoin.Models;

namespace PerfectChannel.Checkout.API.Models.Responses
{
    public class BasketCheckoutResponse : StatusResponse
    {
        public Invoice Invoice { get; set; }

        public static BasketCheckoutResponse CreateSuccess(Invoice invoice)
        {
            var response = new BasketCheckoutResponse()
            {
                Invoice = invoice
            };
            response.MarkSuccess();
            return response;
        }

        public static BasketCheckoutResponse CreateFailure(string message)
        {
            var respone = new BasketCheckoutResponse();
            respone.MarkFailure(message);
            return respone;
        }
    }
}