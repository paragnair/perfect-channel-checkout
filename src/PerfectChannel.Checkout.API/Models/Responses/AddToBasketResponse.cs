﻿using PerfectChannel.Checkout.Applicatoin.Models;

namespace PerfectChannel.Checkout.API.Models.Responses
{
    public class AddToBasketResponse : StatusResponse
    {
        public static AddToBasketResponse CreateFailure(string message)
        {
            return new AddToBasketResponse()
            {
                Message = message
            };
        }

        internal static AddToBasketResponse CreateSuccess(Basket basket)
        {
            return new AddToBasketResponse()
            {
                Success = true,
                Basket = basket
            };
        }

        public Basket Basket { get; set; }
    }
}