﻿using System.Collections.Generic;

namespace PerfectChannel.Checkout.API.Models.Requests
{
    public class AddToBasketRequest
    {
        public string User { get; set; }

        public bool MultipleItems { get; set; }

        public string Item { get; set; }

        public List<AddToBasketRequestItem> Items { get; set; }
    }
}