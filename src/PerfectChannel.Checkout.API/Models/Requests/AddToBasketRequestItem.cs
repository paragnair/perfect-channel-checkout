﻿namespace PerfectChannel.Checkout.API.Models.Requests
{
    public class AddToBasketRequestItem
    {
        public string ItemName { get; set; }

        public int Quantity { get; set; }
    }
}