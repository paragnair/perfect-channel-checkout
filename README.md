# README #

A POC for a simple basket application API.

### How do you compile and run your solution? ###

#### Overview ####
- Unzip the file and Open solution _src/PerfectChannel.Checkout.sln_ in Visual Studio 2015 or later.
- Build the solutions so it restores all NuGet Packages.
- You can run tests by opening the Test Explorer in Visual Studio 2015 and click *Run All*.
- Set **PerfectChannle.Checkout.API** as the start project and run the solution.
- The API runs on port **62171** (it might run on a different port for you).
- Use an application like [Fiddler](http://www.telerik.com/fiddler) or [Postman](https://www.getpostman.com/) to call the API.

#### APi Endpoints

Following are the api Endpoints:

- **/api/products** GET - Returns the list of products
- **/api/basket/bob** GET - Returns the user's basket where **bob** is the user
- **/api/basket/bob/checkout** GET - Checks out the users basket and returns an Invoice where **bob** is the user
- **/api/basket** POST - Add items to basket

Add Items to basket has the following Post Parameters to add a single item:
```javascript
{
	"user" : "bob",
	"item" : "Apples"
}
```

Add Items to basket has the following Post Parameters to add a multiple items:
```javascript
{
	"user" : "bob",
	"multipleItems" : true,
	"items" : [
		{ "itemName" : "Apples", "quantity" : 3 },
		{ "itemName" : "Bread", "quantity" : 2 }
	]
}
```


### What assumptions did you make when went implementing your solution? ###

- The API will be called by a client framework like AngularJS.
- The API will be called securely only from authenticated client so the onus of checking the credentials of the caller is on the calling framework and lets us focus on the business logic.
- There will be no calculation of taxes or discounts (initially).
- The system only supports a single currency hence the current implementation has no currencies, just amounts.
- The current solution uses In-Memory Repositories. So as soon as the app pool is restarted, the data will not persist.
- In the interest of time, the models returned by the API are actual representation of the Domain Objects. In a production application Contracts returned by API will not be the complete representation of Domain Objects.


### Why did you pick the language and design that you did? ###

- ASP.Net WebAPI is a good Microsoft stack to build APIs and gives a lot of fliexibility.
- MVC provides proper separation of concerns and you can build testable layers.
- By making the service return JSON, it can be consumed by client frameworks and the data is light over the network.
- By separating out processes in different interfaces, we can make the system decoupled and make use of dependency injection to manage dependencies. This even allows us to reuse the common functionality across processes.
- Finally, my expertise in ASP.Net MVC & C# and the timescale in which it needed to be completed made me choose these.